"""
URL configuration for systemBackend project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from myapp.views import *
from channels.routing import ProtocolTypeRouter, URLRouter
from myapp import routing as myapp_routing
# from myapp.routing import websocket_urlpatterns


application = ProtocolTypeRouter({
    "websocket": URLRouter(myapp_routing.websocket_urlpatterns),
})

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/testing', admin_testing , name="admin-testing"),

    # Admin apis
    path('api/test', admin_test , name="admin-test-data"),
    path('api/download-dummy-file', download_dummy_file , name="download-dummy-file"),
    path('api/testing', admin_testing , name="admin-testing-data"),
    path('api/node_groups_list', admin_node_groups_list , name="admin-node-groups-data"),
    path('api/node_list_group_filter/<group>', admin_node_list_group_filter , name="admin-node-list-group-filter"),
    path('api/admin/login', admin_login , name="admin-login-data"),
    path('api/admin/node-defination', node_defination , name="single-node-defination"),
    path('api/admin/user-defination', user_defination , name="user-defination"),
    path('api/admin/pbs-create-queue', pbs_create_queue , name="pbs-create-queue"),
    path('api/admin/multiple-node-defination', multiple_node_defination , name="multiple-node-defination"),
    path('api/admin/update-node-defination', update_node_defination , name="update-single-node-defination"),
    path('api/admin/get-node-info/<node>', admin_get_node_info , name="get-node-defination"),
    path('api/admin/get-update-node-info/<node>', admin_get_update_node_info , name="get-update-node-defination"),
    path('api/admin/dm-nodes', admin_delete_multiple_nodes , name="multiple-node-deletion"),
    path('api/admin/ds-node', admin_delete_single_node , name="single-node-deletion"),
    path('api/admin/dm-images', admin_delete_multiple_images , name="multiple-image-deletion"),
    path('api/admin/ds-image', admin_delete_single_image , name="single-image-deletion"),
    path('api/admin/images', get_image_list , name="get-image-list"),
    path('api/admin/get-image-info/<image>', get_image_info , name="get-image-info"),
    path('api/admin/add-image', add_image , name="add-image-info"),
    path('api/admin/update-image', update_image , name="update-image-info"),
    path('api/admin/get-network-info', get_network_info , name="get-network-info"),
    path('api/admin/get-sinfo', get_system_info , name="get-system-info"),
    path('api/admin/get-qstatinfo', get_qstat_info , name="get-qstat-info"),
    path('api/get-system-node-info/<node>', get_system_node_info , name="get-system-node-info"),
    path('api/get-system-pbsnode-info/<node>', get_system_pbsnode_info , name="get-system-pbsnode-info"),
    path('api/get-squeue', get_squeue , name="get-squeue"),
    path('api/get-qstat-squeue', get_qstat_squeue , name="get-qstat-squeue"),
    path('api/get-qstat-user-squeue/<user>', get_qstat_user_squeue , name="get-qstat-squeue"),
    path('api/get-saccount', get_saccount , name="get-saccount"),
    path('api/get-sinfo', get_sinfoR , name="get-sinfoR"),
    path('api/get-jobsinfo', get_jobsinfo , name="get-jobsinfo"),
    path('api/get-tabdump', get_tabdump , name="get-tabdump"),
    path('api/get-usercount', get_userCount , name="get-usercount"),
    path('api/get-systime', get_systemTime , name="get_systemTime"),
    path('api/get-lastlogin', get_lastLogin , name="get-lastLogin"),
    path('api/get-userlist', get_userlist , name="get_userlist"),
    path('api/get-user-full/<uid>', get_user_full , name="get_userlist_full"),
    path('api/delete-user-full/<uid>', delete_user_full , name="delete_user_full"),
    path('api/get-userlist/<usersearch>', get_userlist_search , name="get_userlist_search"),
    path('api/get-jobidData/<jobid>', search_jobId , name="get-jobidData"),
    path('api/get-userjobdata/<specificUserJob>', get_userjobdata , name="get_userjobdata"),
    path('api/get-systemConfiguration', get_systemConfiguration , name="get_systemConfiguration"),
    path('api/get-systemInformation', get_systemInformation , name="get_systemInformation"),
    path('api/sbatch-file', upload_pbs_batch_file , name="upload-pbs-batch-file"),



    path('api/riya-test', riya_test , name="upl"),

]
