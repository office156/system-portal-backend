import asyncio
from django.shortcuts import render
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework import status
from .models import *
from .serializer import *
from django.http import JsonResponse
from rest_framework.decorators import api_view
from .extra_functions import *
from itertools import zip_longest
from django.db.models import F
from django.db.models import Q
from django.db import connection
from django.shortcuts import redirect
from django.urls import reverse
import os
import subprocess
from django.core.files.storage import default_storage
from django.http import FileResponse
from io import BytesIO
import csv
from io import StringIO
import re 
from channels.layers import get_channel_layer
import json
from .websocket_manager import connected_clients





# Create your views here.





# class AmdinLoginDataView(APIView):
#     serializer_class = LiveDataSerializer

#     def get(self,request):
#         detail = [ {"id":detail.id,"date_time": detail.date_time,"user_name": detail.user_name,"Ip_address":detail.Ip_address,"Login_node":detail.Login_node,"wrong_ip":detail.wrong_ip,"wrong_timings":detail.wrong_timings,"invalid_user":detail.invalid_user} 
#         for detail in Live.objects.exclude(user_name = "Na")]
#         # detail = Live.objects.all()
#         return Response(detail)

@api_view(['DELETE'])
def admin_delete_multiple_nodes(request):
        
        nodeList = request.data.get("nodes")
        # # Nodelist.objects.filter(node__in=nodeList).delete()
        print(nodeList)
        # nodes = [ {"node":detail.node,"groups": detail.groups,"status": detail.status} 
        # for detail in Nodelist.objects.all()]
        for node in nodeList:
              command = "rmdef -t node " + node
              os.system(command)

        return Response("Nodes deleted successfully.")

@api_view(['DELETE'])
def admin_delete_single_node(request):
        
        node = request.data.get("node")
        # # Nodelist.objects.filter(node=node).delete()
        # nodes = [ {"node":detail.node,"groups": detail.groups,"status": detail.status} 
        # for detail in Nodelist.objects.all()]
        print(node)

        command = "rmdef -t node " + node
        print(command)
        # try:
        #     os.system(command)
        # except Exception as e:
        #     return HttpResponseServerError("An error occurred: {}".format(e))
        os.system(command)
        return Response("Node deleted successfully.")

@api_view(['DELETE'])
def admin_delete_multiple_images(request):
        
        imageList = request.data.get("images")

        print(imageList)

        for image in imageList:
            image1 = image.split()[0]
            command = "rmdef -t osimage " + image1
            os.system(command)

        return Response("Images deleted successfully.")

@api_view(['DELETE'])
def admin_delete_single_image(request):
        
        image = request.data.get("image")
        image1 = image.split()[0]

        command = "rmdef -t osimage " + image1
        print(command)

        os.system(command)
        return Response("Node deleted successfully.")

@api_view(['GET'])
def admin_node_groups_list(request):
        
        unique_node_groups = Nodelist.objects.values_list('groups').distinct()
        unique_node_array = list(unique_node_groups)
        # print(unique_node_array)

        return Response(unique_node_array)

@api_view(['GET'])
def admin_get_node_info(request,node):
        # node = request.data.get("node")
        print(node)
        print()
        command = "lsdef " + node
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

        # Retrieve the output
        output = result.stdout.strip()
        # print(output)
        lines_output = output.splitlines()
        del lines_output[0]
        print(lines_output)
        result = {item.split('=')[0].strip(): item.split('=')[1] for item in lines_output}
        result2 = []
        result2.append(result)
        return Response(result2)

@api_view(['GET'])
def admin_get_update_node_info(request,node):
        # node = request.data.get("node")
        print(node)
        print()
        query = '''
            SELECT
                nodelist.node,
                nodelist.groups,
                hosts.ip,
                ipmi.username,
                ipmi.password,
                mac.mac,
                nodehm.mgt
            FROM
                nodelist,
                hosts,
                ipmi,
                mac,
                nodehm
            WHERE
                nodelist.node = %s AND
                hosts.node = %s AND
                ipmi.node = %s AND
                mac.node = %s AND
                nodehm.node = %s
        '''
        with connection.cursor() as cursor:
            cursor.execute(query,[node,node,node,node,node])
            result = cursor.fetchall()
        print(result)
        return Response(result)


@api_view(['GET'])
def admin_node_list_group_filter(request,group):
        # print(group)
        # print()

        if group=="all":
              internal_url = reverse(viewname=admin_test)
              return redirect(internal_url)
        query = '''
            SELECT
                nodelist.node,
                nodelist.groups,
                nodelist.status,
                hosts.ip,
                nodetype.os
            FROM
                nodelist
            INNER JOIN
                hosts ON nodelist.node=hosts.node
            INNER JOIN
                nodetype ON hosts.node=nodetype.node
            WHERE
                nodelist.groups = %s
        '''

        with connection.cursor() as cursor:
            cursor.execute(query, [group])
            result = cursor.fetchall()

        # print(result)
        return Response(result)

@api_view(['GET'])
def admin_testing(request):
        command = "lsdef c301"
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

        # Retrieve the output
        output = result.stdout.strip()
        # print(output)
        lines_output = output.splitlines()
        del lines_output[0]
        print(lines_output)
        result = {item.split('=')[0]: item.split('=')[1] for item in lines_output}
        result2 = []
        result2.append(result)
        return Response(result2)

@api_view(['GET'])
def admin_test(request):
        query = '''
            SELECT
                nodelist.node,
                nodelist.groups,
                nodelist.status,
                hosts.ip,
                nodetype.os
            FROM
                nodelist
            INNER JOIN
                hosts ON nodelist.node=hosts.node
            INNER JOIN
                nodetype ON hosts.node=nodetype.node
        '''

        with connection.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        # print(result)
        return Response(result)

# @api_view(['GET'])
# def admin_test(request):
        
        
#         # print(nodes)
#         # nodelist = NodelistDataSerializer(data=nodes)
#         # nodelist.is_valid(raise_exception=True)
#         nodes = [ {"node":detail.node,"groups": detail.groups,"status": detail.status} 
#         for detail in Nodelist.objects.all()]

#         ip = [{"ip":detail.ip} for detail in Hosts.objects.all()]
#         os = [{"os":detail.os} for detail in Nodetype.objects.all()]

#         # ip = []
#         # for nd in nodes :
#         #      print(nd["node"])
#         #      print([{"ip":detail.ip} for detail in Hosts.objects.filter(node=nd["node"])])
#         #      ip.append([{"ip":detail.ip} for detail in Hosts.objects.filter(node=nd["node"])][0] )
        
#         # os = []
#         # for nd in nodes :
#         #      print([{"os":detail.os} for detail in Nodetype.objects.filter(node=nd["node"])])
#         #      os.append([{"os":detail.os} for detail in Nodetype.objects.filter(node=nd["node"])][0] )

    
   

#         combined_array = []

#         for dict1, dict2, dict3 in zip_longest(nodes, os, ip, fillvalue={}):
#             merged_dict = {}
#             merged_dict.update(dict1)
#             merged_dict.update(dict2)
#             merged_dict.update(dict3)
#             combined_array.append(merged_dict)

#         return Response(combined_array)


@api_view(['POST'])
def admin_login(request):
    userName = request.data.get('user')
    passw = request.data.get('password')

    user = [ {"id":detail.id,"password": detail.password,"user_name": detail.user_name} 
    for detail in Admin.objects.filter(user_name = userName, password = passw)]

    if len(user) == 1:
        return Response(status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    
@api_view(['GET'])
def download_dummy_file(request):
    file_path = "/home/cdac/system-portal/system-portal-backend/media/sample.csv"

    with open(file_path, 'r') as file:
        file_content = file.read()
    
    response = HttpResponse(file_content, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="sample.csv"'
    return response



@api_view(['POST'])
def node_defination(request):
    formData = request.data.get('formData')
    ip = formData["ip"]
    hostname = formData["email"]
    groups = formData["groups"]
    managementIp = formData["managementIp"]
    ManagementUsername = formData["ManagementUsername"]
    MacAddress = formData["MacAddress"]
    password = formData["password"]

    command = "mkdef -t node " + hostname +" groups=compute,all" " ip="+ ip +" mac="+ MacAddress+ " netboot=xnba arch=x86_64 mgt=ipmi serialport=0 serialspeed=115200"

    # os.system(command);

    print(formData)
    print(validate_ip(ip))
    if(validate_ip(ip)):
        os.system(command)
        return Response(status=status.HTTP_200_OK)
    else:
        return Response("Incorrect Ip",status=status.HTTP_400_BAD_REQUEST)
    # print(formData.ip)

    # user = [ {"id":detail.id,"password": detail.password,"user_name": detail.user_name} 
    # for detail in Admin.objects.filter(user_name = userName, password = passw)]



    # if len(user) == 1:
    
    # else:
    #     return Response(status=status.HTTP_401_UNAUTHORIZED)

@api_view(['POST'])
def user_defination(request):
    formData = request.data.get('formData')
    print(formData)

    # ipa user-add cent1 --first=CentOS --last=1Linux --email=araj@aiimsb.edu.in --mobile=8987412789 --amount=1000 --application='default brains' --cpuhours=23424 --department='Diagnostics' --designation='professor' --domain='medical' --enddate='dec2025' --funded='paid' --gender=formData["Gender"] --gpuhours=formData["GPUhours"] --institute=formData["Institute"] --pihod=formData["PIHOD"] --projectname=formData["Projectname"] --startdate=formData["Startdate"] --subdomain=formData["Subdomain"] --street=formData["Street"] --city=BKSC --state=Jharkhand --postalcode=formData["Postalcode"] --random

    return Response(status=status.HTTP_200_OK)
    # ip = formData["ip"]
    # hostname = formData["email"]
    # groups = formData["groups"]
    # managementIp = formData["managementIp"]
    # ManagementUsername = formData["ManagementUsername"]
    # MacAddress = formData["MacAddress"]
    # password = formData["password"]

    # command = "mkdef -t node " + hostname +" groups=compute,all" " ip="+ ip +" mac="+ MacAddress+ " netboot=xnba arch=x86_64 mgt=ipmi serialport=0 serialspeed=115200"

    # # os.system(command);

    # print(formData)
    # print(validate_ip(ip))
    # if(validate_ip(ip)):
    #     os.system(command)
    #     return Response(status=status.HTTP_200_OK)
    # else:
    #     return Response("Incorrect Ip",status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def pbs_create_queue(request):
    formData = request.data.get('formData')
    qname = formData["qname"]
    started = formData["started"]
    enabled = formData["enabled"]
    dnodes = formData["dnodes"]
    dwalltime = formData["dwalltime"]
    dqueue = formData["dqueue"]

    command = 'qmgr -c "create queue' +qname +'  queue_type=execution"'
    os.system(command)
    command = 'qmgr -c "set queue' +qname +'  started=' + started +'"'
    os.system(command)
    command = 'qmgr -c "set queue' +qname +'  enabled=' + enabled +'"'
    os.system(command)
    command = 'qmgr -c "set queue' +qname +'  resources_default.nodes=' + dnodes +'"'
    os.system(command)
    command = 'qmgr -c "set queue' +qname +'  resources_default.walltime=' + dwalltime +'"'
    os.system(command)
    command = 'qmgr -c "set server default_queue = ' + qname
    os.system(command)
    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
def multiple_node_defination(request):
    macaddressfile = request.FILES.get("multipleMacAddress")
    groups = request.data.get("multiplegroups")
    manIp = request.data.get("multiplemanagementIp")
    manUser = request.data.get("multipleManagementUsername")
    password = request.data.get("multiplepassword")
    duplicate_macs = []
    duplicate_hostnames = []
    duplicate_ips = []
    # file_path = macaddressfile.temporary_file_path()
    file_name = default_storage.get_available_name(macaddressfile.name)
    default_storage.save(file_name, macaddressfile)
    file_path = default_storage.path(file_name)
    # with open(file_path, 'r') as csvfile:
    #     reader = csv.reader(csvfile)
    #     for row in reader:
    #         print(row[0])
    with open(file_path, 'r') as csvfile:
        csv_reader = csv.reader(csvfile)
        next(csv_reader)
        for row in csv_reader:
            hostname = row[0]
            ip = row[1]
            mac = row[2]

            query = '''
                SELECT
                    COUNT(mac)
                FROM
                    mac
                WHERE
                    LOWER(mac) = LOWER(%s)
            '''
            query1 = '''
                SELECT
                    COUNT(ip)
                FROM
                    hosts
                WHERE
                    LOWER(ip) = LOWER(%s)
            '''
            query2 = '''
                SELECT
                    COUNT(node)
                FROM
                    nodelist
                WHERE
                    LOWER(node) = LOWER(%s)
            '''
            command = "mkdef -t node " + hostname +" groups=" + groups + " ip="+ ip +" mac="+ mac + " netboot=xnba arch=x86_64 mgt=ipmi serialport=0 serialspeed=115200"
            with connection.cursor() as cursor:
                cursor.execute(query,[mac])
                result = cursor.fetchone()[0]
                cursor.execute(query1,[ip])
                result1 = cursor.fetchone()[0]
                cursor.execute(query2,[hostname])
                result2 = cursor.fetchone()[0]
                if int(result) >= 1 :
                        duplicate_macs.append(mac)
                        continue
                elif int(result1) >= 1:
                        duplicate_ips.append(ip)
                elif int(result2) >= 1:
                        duplicate_hostnames.append(hostname)
                else:
                      os.system(command)
            # ...

    # f = open(macaddressfile , "r")
    # print(f.read())


    # with default_storage.open(file_path , 'r') as file:
    #       lines = file.readlines()
    #       for line in lines:
    #             elements = line.split(",")  # Split the line into elements using whitespace as the delimiter
    #             for element in elements:
    #                   print(element)
    default_storage.delete(file_path)

    return Response({'duplicate_macs': duplicate_macs , 'duplicate_ips':duplicate_ips , 'duplicate_hostnames':duplicate_hostnames},status=status.HTTP_200_OK)

@api_view(['POST'])
def upload_pbs_batch_file(request):
    macaddressfile = request.FILES.get("batch_file")

    # file_path = macaddressfile.temporary_file_path()
    file_name = default_storage.get_available_name(macaddressfile.name)
    default_storage.save(file_name, macaddressfile)
    file_path = default_storage.path(file_name)

    return Response("ok")

@api_view(['POST'])
def riya_test(request):
    data_to_send = {"message": "Data from Django to React"}
    channel_layer = get_channel_layer()



    # Send the data to each WebSocket client individually
    async def send_data():
        for client in connected_clients:
            await channel_layer.send(
                client.channel_name,
                {
                    "type": "send_message",
                    "message": json.dumps(data_to_send)
                }
            )

    # asyncio.run(send_data()) not not available bleow python3.7
    
    # Create an event loop and run the send_data function
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(send_data())
    loop.close()

    # Respond with a JSON response
    return JsonResponse({"status": "Data sent to WebSocket clients"})
    # print(request.data)
    # print(request.data['alerts'][0]['labels']['alertname'])

    # return Response("ok")


@api_view(['POST'])
def update_node_defination(request):
    hostname = request.data.get('hostname')
    ip = request.data.get('ip')
    groups = request.data.get('groups')
    managementIp = request.data.get('managementIp')
    MacAddress = request.data.get('MacAddress')
    password = request.data.get('password')
    ManagementUsername = request.data.get('ManagementUsername')


    command = "chdef -t node " + hostname +" groups=" +groups+" ip="+ ip +" mac="+ MacAddress+ " bmc="+ managementIp + " bmcusername="+ ManagementUsername + " bmcpassword="+ password + " netboot=xnba arch=x86_64 mgt=ipmi serialport=0 serialspeed=115200"

    # # os.system(command);

    # print(formData)
    # print(validate_ip(ip))
    # print(hostname)
    # print(groups)
    # print(ip)
    # print(managementIp)
    # print(MacAddress)
    # print(password)
    # print(ManagementUsername)

    if(validate_ip(ip)):
        os.system(command)
        return Response(status=status.HTTP_200_OK)
        # return Response("Successfully called update node")
    else:
        return Response("Incorrect Ip",status=status.HTTP_400_BAD_REQUEST)

    


@api_view(['GET'])
def get_image_list(request):
    command = "lsdef -t osimage"
    result = subprocess.check_output(command, shell=True).decode().splitlines()
    print(result)
    return Response({'image_list': result },status=status.HTTP_200_OK)

@api_view(['GET'])
def get_image_info(request,image):
    # print(image.replace(" (osimage)",""))
    image1 = image.split()[0]
    print(image1)
    command = "lsdef -t osimage " + image1
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    # print(output)
    lines_output = output.splitlines()
    del lines_output[0]
    print(lines_output)
    result = {item.split('=')[0]: item.split('=')[1] for item in lines_output}
    result2 = []
    result2.append(result)
    return Response(result2)
    # return Response("ok")
    
    # result = subprocess.check_output(command, shell=True).decode().splitlines()
    # print(result)
    # return Response({'image_list': result },status=status.HTTP_200_OK)

@api_view(['POST'])
def add_image(request):
    image_path = request.data.get('selectedFile')
    print(image_path)
    print()
    print("In add image")
    print()
    image_path_test = "/install/alma8.7/Sankh-AlmaLinux-8.7-20230530.iso"
    command = "copycds " + image_path_test
    os.system(command)
    return Response(status=status.HTTP_200_OK)

@api_view(['POST'])
def update_image(request):
    print("In update image.")
    exlist = request.FILES.get("file1")
    pkglist = request.FILES.get("file2")
    default_storage.save(exlist.name, exlist)
    default_storage.save(pkglist.name, pkglist)
    # print(request.data)
    return Response(status=status.HTTP_200_OK)

# @api_view(['GET'])
# def get_image_info(request,image):
#     # print(image.replace(" (osimage)",""))
#     image1 = image.split()[0]
#     print(image1)
#     command = "lsdef -t osimage " + image1
#     result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
#     # Retrieve the output
#     output = result.stdout.strip()
#     # print(output)
#     lines_output = output.splitlines()
#     del lines_output[0]
#     print(lines_output)
#     # lines_output1 = [item.split()[1] for item in lines_output]
#     result = {item.split('=')[0]: item.split('=')[1] for item in lines_output}
#     result2 = []
#     # result2.append(result)
#     stripped_dict = {key.strip(): value for key, value in result.items()}
#     result2.append(stripped_dict)
#     print()
#     print(result2)
#     return Response(result2)
#     # return Response("ok")
    
#     # result = subprocess.check_output(command, shell=True).decode().splitlines()
#     # print(result)
#     # return Response({'image_list': result },status=status.HTTP_200_OK)


@api_view(['GET'])
def get_network_info(request):
    command = "nmcli"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    # print(output)
    lines_output = output.split("\n\n")
    multidimensional_array = [paragraph.split('\n') for paragraph in lines_output]
    print(multidimensional_array)
    # result = {item.split('=')[0]: item.split('=')[1] for item in lines_output}
    # result2 = []
    # result2.append(result)
    # print(result2)
    return Response(multidimensional_array)

@api_view(['GET'])
def get_system_info(request):
    command = "sinfo"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    # print(output)
    lines_output = output.splitlines()
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    print(multi_array)
    return Response(multi_array)

@api_view(['GET'])
def get_qstat_info(request):
    command = "qstat -B"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    # print(output)
    lines_output = output.splitlines()
    del lines_output[0]
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    print(multi_array)
    return Response(multi_array)


@api_view(['GET'])
def get_system_node_info(request,node):
    command = "scontrol show node " + node
    # result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # # Retrieve the output
    # output = result.stdout.strip()
    # lines_output = output.splitlines()
    # print(lines_output)
    # del lines_output[0]
    # result = {item.split('=')[0]: item.split('=')[1] for item in lines_output}
    # result2 = []
    # result2.append(result)
    # print(result2)

    # return Response(result2)
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    output = result.stdout.strip()
    lines_output = output.splitlines()
    # multidimensional_array = [paragraph.split('\n') for paragraph in lines_output]
    # print(multidimensional_array)

    return Response(lines_output)

def remove(string):
    return string.replace(" ", "")

@api_view(['GET'])
def get_system_pbsnode_info(request,node):
    command = "pbsnodes " + node
    # print(command)
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    # print(lines_output)
    res = []
    for ele in lines_output:
        # print(ele)
        # if ele.strip():
        #     print(ele)
        ele2 = remove(ele)
        res.append(ele2)
    result = {item.split('=')[0]: item.split('=')[1] for item in res}
    print(result)
    result.pop("ntype")
    result.pop("resources_available.vnode")
    result.pop("resources_assigned.accelerator_memory")
    result.pop("resources_available.ncpus")
    result.popitem()
    result.popitem()
    result.popitem()
    result.popitem()
    result.popitem()
    result.popitem()
    result.popitem()
    result.popitem()
    result.popitem()
    # result.popitem()
    
    result2 = []
    result2.append(result)
    # print(result2)


    return Response(result2)

@api_view(['GET'])
def get_squeue(request):
    command = "squeue"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    keys = ['key', 'jobId', 'partition' , 'name' , 'user' , 'st' , 'time' , 'nodes' , 'nodelist']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    return Response(result)

@api_view(['GET'])
def get_qstat_squeue(request):
    command = "qstat -a"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    del lines_output[0]
    del lines_output[0]
    del lines_output[0]
    multi_array = []

    print(lines_output)

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    keys = ['jobId' , 'name' , 'queue' , 'jobname' , 'sessionid' , 'nds' , 'tsk' , 'memory' , 'time' , 'state' , 'left_time']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    return Response(result)

@api_view(['GET'])
def get_qstat_user_squeue(request,user):
    command = "qstat -u " + user
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    del lines_output[0]
    del lines_output[0]
    del lines_output[0]
    multi_array = []

    print(lines_output)

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    keys = ['jobId' , 'name' , 'queue' , 'jobname' , 'sessionid' , 'nds' , 'tsk' , 'memory' , 'time' , 'state' , 'left_time']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    return Response(result)




@api_view(['GET'])
def get_sinfoR(request):
    command = "sinfo -R"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    keys = ['reason', 'user', 'timestamp' , 'nodelist']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    print(result)

    return Response(result)

@api_view(['GET'])
def get_userlist(request):
    # command = "ldapsearch -x -LLL uid=cent1 | grep -E 'uid:|loginShell|uidNumber|gidNumber|homeDirectory' | head -n 5"
    command = "ldapsearch -H ldap://$HOSTNAME -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -x -LLL uid | grep -E 'uid:'"
    # command = "ldapsearch -H ldap://$HOSTNAME -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -x -LLL | grep -E 'cn:'"
    result = []


    result1 = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result1.stdout.strip()
    lines_output = output.splitlines()
    for line in lines_output:
        elements = line.split(":")
        if len(elements) > 0:
            result.append(elements[1])
    
    print(result)
    
    final_result = []
    for user in result:
         print("first for loop")
         result2 = {}
         command1 = "ldapsearch -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -LLL uid=" + user.strip() + " | grep -E 'uid:|displayName:|loginShell|uidNumber|gidNumber|homeDirectory|subdomain|startdate|projectname|pihod|institute|gpuhours|gender|funded|enddate|domain|designation|department|cpuhours|application|amount|amount|application|amount|mobile|postalCode|st:|street|l:'"
         print(command1)
         lines = subprocess.run(command1, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
         lines2 = lines.stdout.strip().splitlines()

         for line in lines2:
                elements = line.split(":")
                print("inside for loop")
                print(elements)
                if len(elements) >= 2:
                    key = elements[0]
                    value = elements[1]
                    result2[key] = value
                    print(result2)

         final_result.append(result2)

    print(final_result)



    return Response(final_result)

# @api_view(['GET'])
# def get_userlist(request):
#     # command = "ldapsearch -x -LLL uid=cent1 | grep -E 'uid:|loginShell|uidNumber|gidNumber|homeDirectory' | head -n 5"
#     command = "ldapsearch -H ldap://$HOSTNAME -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -x -LLL uid | grep -E 'uid:'"
#     # command = "ldapsearch -H ldap://$HOSTNAME -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -x -LLL | grep -E 'cn:'"
#     result = []


#     result1 = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
#     # Retrieve the output
#     output = result1.stdout.strip()
#     lines_output = output.splitlines()
#     for line in lines_output:
#         elements = line.split(":")
#         if len(elements) > 0:
#             result.append(elements[1])
    
#     print(result)
    
#     final_result = []
#     for user in result:
#          print("first for loop")
#          result2 = {}
#          command1 = "ldapsearch -H ldap://$HOSTNAME -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -x -LLL uid=" + user.strip() + " | grep -E 'uid:|displayName:|loginShell|uidNumber|gidNumber|homeDirectory'"
#          print(command1)
#          lines = subprocess.run(command1, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
#          lines2 = lines.stdout.strip().splitlines()

#          for line in lines2:
#                 elements = line.split(":")
#                 print("inside for loop")
#                 print(elements)
#                 if len(elements) >= 2:
#                     key = elements[0]
#                     value = elements[1]
#                     result2[key] = value
#                     print(result2)

#          final_result.append(result2)

#     print(final_result)



#     return Response(final_result)


@api_view(['GET'])
def get_user_full(request,uid):
     command = "ipa user-show " + uid

     result1 = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
     output = result1.stdout.strip()
     lines_output = output.splitlines()

     return Response(lines_output)

@api_view(['GET'])
def delete_user_full(request,uid):
     command = "ipa user-del " + uid

     result1 = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
     output = result1.stdout.strip()
     lines_output = output.splitlines()
     response = lines_output[1]

     return Response(response)




# @api_view(['GET'])
# def get_userlist_full(request):
#     # command = "ldapsearch -x -LLL uid=cent1 | grep -E 'uid:|loginShell|uidNumber|gidNumber|homeDirectory' | head -n 5"
#     command = "ldapsearch -H ldap://$HOSTNAME -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -x -LLL uid | grep -E 'uid:'"
#     # command = "ldapsearch -H ldap://$HOSTNAME -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -x -LLL | grep -E 'cn:'"
#     result = []


#     result1 = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
#     # Retrieve the output
#     output = result1.stdout.strip()
#     lines_output = output.splitlines()
#     for line in lines_output:
#         elements = line.split(":")
#         if len(elements) > 0:
#             result.append(elements[1])
    
#     print(result)
    
#     final_result = []
#     for user in result:
#          print("first for loop")
#          result2 = {}
#          command1 = "ipa user-show " + user.strip()
#          print(command1)
#          lines = subprocess.run(command1, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
#          lines2 = lines.stdout.strip().splitlines()

#          for line in lines2:
#                 elements = line.split(":")
#                 print("inside for loop")
#                 print(elements)
#                 if len(elements) >= 2:
#                     key = elements[0]
#                     value = elements[1]
#                     result2[key] = value
#                     print(result2)

#          final_result.append(result2)

#     print(final_result)



#     return Response(final_result)

@api_view(['GET'])
def get_userlist_search(request,usersearch):
    # command = "ldapsearch -x -LLL uid=cent1 | grep -E 'uid:|loginShell|uidNumber|gidNumber|homeDirectory' | head -n 5"
    command = "ldapsearch -H ldap://$HOSTNAME -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -x -LLL uid | grep -E 'uid:'"
    result = []


    result1 = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result1.stdout.strip()
    lines_output = output.splitlines()
    for line in lines_output:
        elements = line.split(":")
        if len(elements) > 0:
            result.append(elements[1])
    
    print(result)
    
    final_result = []
    for user in result:
         
         if user.__contains__(usersearch):
              print("first for loop")
              result2 = {}
              command1 = "ldapsearch -H ldap://$HOSTNAME -b 'cn=users,cn=accounts,dc=master,dc=cd,dc=in' -x -LLL uid=" + user.strip() + " | grep -E 'uid:|displayName:|loginShell|uidNumber|gidNumber|homeDirectory'"
              print(command1)
              lines = subprocess.run(command1, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
              lines2 = lines.stdout.strip().splitlines()

              for line in lines2:
                elements = line.split(":")
                print("inside for loop")
                print(elements)
                if len(elements) >= 2:
                    key = elements[0]
                    value = elements[1]
                    result2[key] = value
                    print(result2)
               
              final_result.append(result2)


    print(final_result)



    return Response(final_result)

@api_view(['GET'])
def get_jobsinfo(request):
    command = "sacct --starttime 2014-07-01 --format=User,JobID,Jobname,partition,state,time,start,end,elapsed,MaxRss,MaxVMSize,nnodes,ncpus,nodelist"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        stripped_string = string.strip()
        substrings = re.split(r'\s+', stripped_string)
        multi_array.append(substrings)

    keys = ['user', 'jobid', 'jobname' , 'partition' , 'state' , 'timelimit' , 'start' , 'end' , 'elapsed' , 'maxrss' , 'maxvmsize' , 'nnodes' , 'ncpus' , 'nodelist']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    return Response(result)

@api_view(['GET'])
def get_tabdump(request):
    command = "tabdump networks"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        stripped_string = string.strip()
        substrings = re.split(',', stripped_string)
        multi_array.append(substrings)

    keys = ['netname', 'net', 'mask' , 'mgtifname' , 'gateway' , 'dhcpserver' , 'tftpserver' , 'nameservers' , 'ntpservers' , 'logservers' , 'dynamicrange' , 'staticrange' , 'staticrangeincrement' , 'nodehostname' , 'ddnsdomain' , 'vlanid' , 'domain' , 'mtu' , 'comments' , 'disable']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    return Response(result)

@api_view(['GET'])
def get_userCount(request):
    command = "ldapsearch -x -LLL | grep cn=users,cn=accounts,dc=master,dc=cd,dc=in | grep dn: | wc -l"
    result = subprocess.check_output(command, shell=True)
    output = result.decode('utf-8')
    print(output)

    return Response(output)

@api_view(['GET'])
def get_lastLogin(request):
    command = "last | head -n 1"
    result = subprocess.check_output(command, shell=True)
    output = result.decode('utf-8')
    print(output)

    return Response(output)

@api_view(['GET'])
def get_systemTime(request):
    command = "date"
    result = subprocess.check_output(command, shell=True)
    output = result.decode('utf-8')
    print(output)

    return Response(output)

@api_view(['GET'])
def search_jobId(request,jobid):
    command = "sacct -j " + jobid
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        stripped_string = string.strip()
        substrings = re.split(r'\s+', stripped_string)
        multi_array.append(substrings)

    keys = ['jobid', 'jobname' , 'partition' , 'account', 'alloccpus','state' ,'exitcode']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    print(result)

    return Response(result)

@api_view(['GET'])
def get_userjobdata(request,specificUserJob):
    command = "squeue -u " + specificUserJob
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        stripped_string = string.strip()
        substrings = re.split(r'\s+', stripped_string)
        multi_array.append(substrings)

    keys = ['jobid', 'partition' , 'name' , 'user' , 'st', 'time', 'nodes','nodelist']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    print(result)

    return Response(result)

@api_view(['GET'])
def get_systemConfiguration(request):
    command = "hostnamectl"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    multi_array = []
    keys = ['hostname', 'transient' , 'iconname' , 'chassis' , 'machineid', 'bootid', 'operatingsystem','cpeosname' ,'kernel' , 'arch']
    data = {}

    for i,line in enumerate(lines_output):
        key = keys[i]
        value = line.split(':', 1)[1].strip()
        data[key] = value
    
    multi_array.append(data)


    return Response(multi_array)

@api_view(['GET'])
def get_systemInformation(request):
    command = "sudo dmidecode -t system | grep -E 'Manufacturer:|Name:|Version:|Number:|UUID:|Type:|SKU Number:|Family:'"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    multi_array = []
    keys = ['manufacturer', 'productname' , 'version' , 'serialno' , 'uuid', 'wakeuptype', 'skuno','family']
    data = {}

    for i,line in enumerate(lines_output):
        key = keys[i]
        value = line.split(':', 1)[1].strip()
        data[key] = value
    
    multi_array.append(data)


    return Response(multi_array)

@api_view(['GET'])
def get_saccount(request):
    command = "sacctmgr show accounts"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()
    del lines_output[0]
    del lines_output[0]

    multi_array = []

    for string in lines_output:
        string.strip()
        words = re.split(r'\s+', string)
        print(words)
        first_word = words[1]
        last_word = words[-2]
        remaining_words = ' '.join(words[2:-2])
        sublist = []
        sublist.append(first_word)
        sublist.append(remaining_words)
        sublist.append(last_word)
        multi_array.append(sublist)
        print(multi_array)
    
    keys = ['account', 'descr', 'org']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    return Response(result)