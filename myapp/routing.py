# routing.py

from django.urls import re_path
from channels.routing import ProtocolTypeRouter, URLRouter
from . import consumers

websocket_urlpatterns = [
    re_path(r'ws/riya_test/$', consumers.MyConsumer.as_asgi()),
]

# application = ProtocolTypeRouter({
#     "websocket": URLRouter(websocket_urlpatterns),
# })