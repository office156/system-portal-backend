from rest_framework import serializers
from . models import *

class LiveDataSerializer(serializers.ModelSerializer):
	class Meta:
		model = Admin
		fields = ['id','user_name', 'password']


class NodelistDataSerializer(serializers.ModelSerializer):
	class Meta:
		model = Nodelist
		fields = ['node','groups','status' , 'statustime','appstatus','appstatustime','primarysn','hidden','updatestatus','updatestatustime','zonename','comments','disable']
		
# class PreviousDataSerializer(serializers.ModelSerializer):
# 	class Meta:
# 		model = Previous
# 		fields = ['id','date_time', 'user_name','Ip_address','Login_node','wrong_ip','wrong_timings','invalid_user']


# class FlagDataSerializer(serializers.ModelSerializer):
# 	class Meta:
# 		model = Flag
# 		fields = ['id','date_time', 'user_name','Ip_address','Login_node','wrong_ip','wrong_timings','invalid_user','Flag']
