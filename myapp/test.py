from itertools import zip_longest

array1 = [{'name': 'John'}, {'age': 25}, {'country': 'USA'}, {'language': 'English'}]
array2 = [{'occupation': 'Engineer'}, {'language': 'Python'}, {'experience': '5 years'}]
array3 = [{'hobby': 'Soccer'}, {'skill': 'Programming'}, {'interest': 'Travel'}, {'hobby': 'Cooking'}]

combined_array = []

for dict1, dict2, dict3 in zip_longest(array1, array2, array3, fillvalue={}):
    merged_dict = {}
    merged_dict.update(dict1)
    merged_dict.update(dict2)
    merged_dict.update(dict3)
    combined_array.append(merged_dict)

print(combined_array)
