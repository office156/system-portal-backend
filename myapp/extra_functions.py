import re
import os

def validate_ip(s):  
       
    # check number of periods  
    if s.count('.') != 3:  
        return False  
   
    ip_list = list(map(str, s.split('.')))  
   
    # check range of each number between periods  
    for element in ip_list:  
        if int(element) < 0 or int(element) > 255 or (element[0]=='0' and len(element)!=1):  
            return False  
   
    return True  


def validate_mac(str):
    #regural expression
    regex = ("^([0-9A-Fa-f]{2}[:-])" +"{5}([0-9A-Fa-f]{2})|" +"([0-9a-fA-F]{4}\\." +"[0-9a-fA-F]{4}\\." +"[0-9a-fA-F]{4})$")
    match = re.compile(regex)
    if (str == None):
        return False
        if(re.search(match, str)):
            return True
    else:
        return False
