# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Auditlog(models.Model):
    recid = models.AutoField(primary_key=True)
    audittime = models.TextField(blank=True, null=True)
    userid = models.TextField(blank=True, null=True)
    clientname = models.TextField(blank=True, null=True)
    clienttype = models.TextField(blank=True, null=True)
    command = models.TextField(blank=True, null=True)
    noderange = models.TextField(blank=True, null=True)
    args = models.TextField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'auditlog'

class Alerts(models.Model):
    primary = models.TextField(primary_key=True,auto_created=True)
    alertName = models.TextField(blank=True, null=True)
    alertInstance = models.TextField(blank=True, null=True)
    summary = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    alertTime = models.TimeField(blank=True, null=True)
    # clienttype = models.TextField(blank=True, null=True)
    # command = models.TextField(blank=True, null=True)
    # noderange = models.TextField(blank=True, null=True)
    # args = models.TextField(blank=True, null=True)
    # status = models.TextField(blank=True, null=True)
    # comments = models.TextField(blank=True, null=True)
    # disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'alerts'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Bootparams(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    kernel = models.TextField(blank=True, null=True)
    initrd = models.TextField(blank=True, null=True)
    kcmdline = models.TextField(blank=True, null=True)
    addkcmdline = models.TextField(blank=True, null=True)
    dhcpstatements = models.TextField(blank=True, null=True)
    adddhcpstatements = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bootparams'


class Boottarget(models.Model):
    bprofile = models.CharField(primary_key=True, max_length=128)
    kernel = models.TextField(blank=True, null=True)
    initrd = models.TextField(blank=True, null=True)
    kcmdline = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'boottarget'


class Cfgmgt(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    cfgmgr = models.TextField(blank=True, null=True)
    cfgserver = models.TextField(blank=True, null=True)
    roles = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cfgmgt'


class Chain(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    currstate = models.TextField(blank=True, null=True)
    currchain = models.TextField(blank=True, null=True)
    chain = models.TextField(blank=True, null=True)
    ondiscover = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chain'


class Deps(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    nodedep = models.TextField(blank=True, null=True)
    msdelay = models.TextField(blank=True, null=True)
    cmd = models.CharField(max_length=128)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'deps'
        unique_together = (('node', 'cmd'),)


class Discoverydata(models.Model):
    uuid = models.CharField(primary_key=True, max_length=128)
    node = models.TextField(blank=True, null=True)
    method = models.TextField(blank=True, null=True)
    discoverytime = models.TextField(blank=True, null=True)
    arch = models.TextField(blank=True, null=True)
    cpucount = models.TextField(blank=True, null=True)
    cputype = models.TextField(blank=True, null=True)
    memory = models.TextField(blank=True, null=True)
    mtm = models.TextField(blank=True, null=True)
    serial = models.TextField(blank=True, null=True)
    nicdriver = models.TextField(blank=True, null=True)
    nicipv4 = models.TextField(blank=True, null=True)
    nichwaddr = models.TextField(blank=True, null=True)
    nicpci = models.TextField(blank=True, null=True)
    nicloc = models.TextField(blank=True, null=True)
    niconboard = models.TextField(blank=True, null=True)
    nicfirm = models.TextField(blank=True, null=True)
    switchname = models.TextField(blank=True, null=True)
    switchaddr = models.TextField(blank=True, null=True)
    switchdesc = models.TextField(blank=True, null=True)
    switchport = models.TextField(blank=True, null=True)
    otherdata = models.CharField(max_length=2048, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discoverydata'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Domain(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    ou = models.TextField(blank=True, null=True)
    authdomain = models.TextField(blank=True, null=True)
    adminuser = models.TextField(blank=True, null=True)
    adminpassword = models.TextField(blank=True, null=True)
    type = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'domain'


class Eventlog(models.Model):
    recid = models.AutoField(primary_key=True)
    eventtime = models.TextField(blank=True, null=True)
    eventtype = models.TextField(blank=True, null=True)
    monitor = models.TextField(blank=True, null=True)
    monnode = models.TextField(blank=True, null=True)
    node = models.TextField(blank=True, null=True)
    application = models.TextField(blank=True, null=True)
    component = models.TextField(blank=True, null=True)
    id = models.TextField(blank=True, null=True)
    severity = models.TextField(blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    rawdata = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'eventlog'


class Firmware(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    cfgfile = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'firmware'


class Hosts(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    ip = models.TextField(blank=True, null=True)
    hostnames = models.TextField(blank=True, null=True)
    otherinterfaces = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'hosts'


class Hwinv(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    cputype = models.TextField(blank=True, null=True)
    cpucount = models.TextField(blank=True, null=True)
    memory = models.TextField(blank=True, null=True)
    disksize = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'hwinv'


class Hypervisor(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    type = models.TextField(blank=True, null=True)
    mgr = models.TextField(blank=True, null=True)
    interface = models.TextField(blank=True, null=True)
    netmap = models.TextField(blank=True, null=True)
    defaultnet = models.TextField(blank=True, null=True)
    cluster = models.TextField(blank=True, null=True)
    datacenter = models.TextField(blank=True, null=True)
    preferdirect = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'hypervisor'


class Ipmi(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    bmc = models.TextField(blank=True, null=True)
    bmcport = models.TextField(blank=True, null=True)
    taggedvlan = models.TextField(blank=True, null=True)
    bmcid = models.TextField(blank=True, null=True)
    username = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ipmi'


class Iscsi(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    server = models.TextField(blank=True, null=True)
    target = models.TextField(blank=True, null=True)
    lun = models.TextField(blank=True, null=True)
    iname = models.TextField(blank=True, null=True)
    file = models.TextField(blank=True, null=True)
    userid = models.TextField(blank=True, null=True)
    passwd = models.TextField(blank=True, null=True)
    kernel = models.TextField(blank=True, null=True)
    kcmdline = models.TextField(blank=True, null=True)
    initrd = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'iscsi'


class Kit(models.Model):
    kitname = models.CharField(primary_key=True, max_length=128)
    basename = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    version = models.TextField(blank=True, null=True)
    release = models.TextField(blank=True, null=True)
    ostype = models.TextField(blank=True, null=True)
    isinternal = models.TextField(blank=True, null=True)
    kitdeployparams = models.TextField(blank=True, null=True)
    kitdir = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kit'


class Kitcomponent(models.Model):
    kitcompname = models.CharField(primary_key=True, max_length=128)
    description = models.TextField(blank=True, null=True)
    kitname = models.TextField(blank=True, null=True)
    kitreponame = models.TextField(blank=True, null=True)
    basename = models.TextField(blank=True, null=True)
    version = models.TextField(blank=True, null=True)
    release = models.TextField(blank=True, null=True)
    serverroles = models.TextField(blank=True, null=True)
    kitpkgdeps = models.TextField(blank=True, null=True)
    prerequisite = models.TextField(blank=True, null=True)
    driverpacks = models.TextField(blank=True, null=True)
    kitcompdeps = models.TextField(blank=True, null=True)
    postbootscripts = models.TextField(blank=True, null=True)
    genimage_postinstall = models.TextField(blank=True, null=True)
    exlist = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kitcomponent'


class Kitrepo(models.Model):
    kitreponame = models.CharField(primary_key=True, max_length=128)
    kitname = models.TextField(blank=True, null=True)
    osbasename = models.TextField(blank=True, null=True)
    osmajorversion = models.TextField(blank=True, null=True)
    osminorversion = models.TextField(blank=True, null=True)
    osarch = models.TextField(blank=True, null=True)
    compat_osbasenames = models.TextField(blank=True, null=True)
    kitrepodir = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kitrepo'


class KvmMasterdata(models.Model):
    name = models.CharField(primary_key=True, max_length=128)
    xml = models.CharField(max_length=16000, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kvm_masterdata'


class KvmNodedata(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    xml = models.CharField(max_length=16000, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kvm_nodedata'


class Linuximage(models.Model):
    imagename = models.CharField(primary_key=True, max_length=128)
    template = models.TextField(blank=True, null=True)
    boottarget = models.TextField(blank=True, null=True)
    addkcmdline = models.TextField(blank=True, null=True)
    pkglist = models.TextField(blank=True, null=True)
    pkgdir = models.TextField(blank=True, null=True)
    otherpkglist = models.TextField(blank=True, null=True)
    otherpkgdir = models.TextField(blank=True, null=True)
    exlist = models.TextField(blank=True, null=True)
    postinstall = models.TextField(blank=True, null=True)
    rootimgdir = models.TextField(blank=True, null=True)
    kerneldir = models.TextField(blank=True, null=True)
    nodebootif = models.TextField(blank=True, null=True)
    otherifce = models.TextField(blank=True, null=True)
    netdrivers = models.TextField(blank=True, null=True)
    kernelver = models.TextField(blank=True, null=True)
    krpmver = models.TextField(blank=True, null=True)
    permission = models.TextField(blank=True, null=True)
    dump = models.TextField(blank=True, null=True)
    crashkernelsize = models.TextField(blank=True, null=True)
    partitionfile = models.TextField(blank=True, null=True)
    driverupdatesrc = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'linuximage'


class Litefile(models.Model):
    image = models.CharField(primary_key=True, max_length=128)
    file = models.CharField(max_length=128)
    options = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'litefile'
        unique_together = (('image', 'file'),)


class Litetree(models.Model):
    priority = models.CharField(primary_key=True, max_length=128)
    image = models.TextField(blank=True, null=True)
    directory = models.TextField()
    mntopts = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'litetree'


class Mac(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    interface = models.TextField(blank=True, null=True)
    mac = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mac'


class Mic(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    host = models.TextField(blank=True, null=True)
    id = models.TextField(blank=True, null=True)
    nodetype = models.TextField(blank=True, null=True)
    bridge = models.TextField(blank=True, null=True)
    onboot = models.TextField(blank=True, null=True)
    vlog = models.TextField(blank=True, null=True)
    powermgt = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mic'


class Monitoring(models.Model):
    name = models.CharField(primary_key=True, max_length=128)
    nodestatmon = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'monitoring'


class Monsetting(models.Model):
    name = models.CharField(primary_key=True, max_length=128)
    key = models.CharField(max_length=128)
    value = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'monsetting'
        unique_together = (('name', 'key'),)


class Mp(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    mpa = models.TextField(blank=True, null=True)
    id = models.TextField(blank=True, null=True)
    nodetype = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mp'


class Mpa(models.Model):
    mpa = models.CharField(primary_key=True, max_length=128)
    username = models.CharField(max_length=128)
    password = models.TextField(blank=True, null=True)
    displayname = models.TextField(blank=True, null=True)
    slots = models.TextField(blank=True, null=True)
    urlpath = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mpa'
        unique_together = (('mpa', 'username'),)


class Admin(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_name = models.CharField(max_length=30)
    password = models.CharField(max_length=600)

    class Meta:
        managed = False
        db_table = 'myapp_admin'


class Networks(models.Model):
    netname = models.TextField(blank=True, null=True)
    net = models.CharField(primary_key=True, max_length=128)
    mask = models.CharField(max_length=128)
    mgtifname = models.TextField(blank=True, null=True)
    gateway = models.TextField(blank=True, null=True)
    dhcpserver = models.TextField(blank=True, null=True)
    tftpserver = models.TextField(blank=True, null=True)
    nameservers = models.TextField(blank=True, null=True)
    ntpservers = models.TextField(blank=True, null=True)
    logservers = models.TextField(blank=True, null=True)
    dynamicrange = models.TextField(blank=True, null=True)
    staticrange = models.TextField(blank=True, null=True)
    staticrangeincrement = models.TextField(blank=True, null=True)
    nodehostname = models.TextField(blank=True, null=True)
    ddnsdomain = models.TextField(blank=True, null=True)
    vlanid = models.TextField(blank=True, null=True)
    domain = models.TextField(blank=True, null=True)
    mtu = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'networks'
        unique_together = (('net', 'mask'),)


class Nics(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    nicips = models.TextField(blank=True, null=True)
    nichostnamesuffixes = models.TextField(blank=True, null=True)
    nichostnameprefixes = models.TextField(blank=True, null=True)
    nictypes = models.TextField(blank=True, null=True)
    niccustomscripts = models.TextField(blank=True, null=True)
    nicnetworks = models.TextField(blank=True, null=True)
    nicaliases = models.TextField(blank=True, null=True)
    nicextraparams = models.TextField(blank=True, null=True)
    nicdevices = models.TextField(blank=True, null=True)
    nicsadapter = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nics'


class Nimimage(models.Model):
    imagename = models.CharField(primary_key=True, max_length=128)
    nimtype = models.TextField(blank=True, null=True)
    lpp_source = models.TextField(blank=True, null=True)
    spot = models.TextField(blank=True, null=True)
    root = models.TextField(blank=True, null=True)
    dump = models.TextField(blank=True, null=True)
    paging = models.TextField(blank=True, null=True)
    resolv_conf = models.TextField(blank=True, null=True)
    tmp = models.TextField(blank=True, null=True)
    home = models.TextField(blank=True, null=True)
    shared_home = models.TextField(blank=True, null=True)
    res_group = models.TextField(blank=True, null=True)
    nimmethod = models.TextField(blank=True, null=True)
    script = models.TextField(blank=True, null=True)
    bosinst_data = models.TextField(blank=True, null=True)
    installp_bundle = models.TextField(blank=True, null=True)
    mksysb = models.TextField(blank=True, null=True)
    fb_script = models.TextField(blank=True, null=True)
    shared_root = models.TextField(blank=True, null=True)
    otherpkgs = models.TextField(blank=True, null=True)
    image_data = models.TextField(blank=True, null=True)
    configdump = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nimimage'


class Nodegroup(models.Model):
    groupname = models.CharField(primary_key=True, max_length=128)
    grouptype = models.TextField(blank=True, null=True)
    members = models.TextField(blank=True, null=True)
    membergroups = models.TextField(blank=True, null=True)
    wherevals = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nodegroup'


class Nodehm(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    power = models.TextField(blank=True, null=True)
    mgt = models.TextField(blank=True, null=True)
    cons = models.TextField(blank=True, null=True)
    termserver = models.TextField(blank=True, null=True)
    termport = models.TextField(blank=True, null=True)
    conserver = models.TextField(blank=True, null=True)
    serialport = models.TextField(blank=True, null=True)
    serialspeed = models.TextField(blank=True, null=True)
    serialflow = models.TextField(blank=True, null=True)
    getmac = models.TextField(blank=True, null=True)
    cmdmapping = models.TextField(blank=True, null=True)
    consoleondemand = models.TextField(blank=True, null=True)
    consoleenabled = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nodehm'


class Nodelist(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    groups = models.TextField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)
    statustime = models.TextField(blank=True, null=True)
    appstatus = models.TextField(blank=True, null=True)
    appstatustime = models.TextField(blank=True, null=True)
    primarysn = models.TextField(blank=True, null=True)
    hidden = models.TextField(blank=True, null=True)
    updatestatus = models.TextField(blank=True, null=True)
    updatestatustime = models.TextField(blank=True, null=True)
    zonename = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nodelist'


class Nodepos(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    rack = models.TextField(blank=True, null=True)
    u = models.TextField(blank=True, null=True)
    chassis = models.TextField(blank=True, null=True)
    slot = models.TextField(blank=True, null=True)
    room = models.TextField(blank=True, null=True)
    height = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nodepos'


class Noderes(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    servicenode = models.TextField(blank=True, null=True)
    netboot = models.TextField(blank=True, null=True)
    tftpserver = models.TextField(blank=True, null=True)
    tftpdir = models.TextField(blank=True, null=True)
    nfsserver = models.TextField(blank=True, null=True)
    monserver = models.TextField(blank=True, null=True)
    nfsdir = models.TextField(blank=True, null=True)
    installnic = models.TextField(blank=True, null=True)
    primarynic = models.TextField(blank=True, null=True)
    discoverynics = models.TextField(blank=True, null=True)
    cmdinterface = models.TextField(blank=True, null=True)
    xcatmaster = models.TextField(blank=True, null=True)
    current_osimage = models.TextField(blank=True, null=True)
    next_osimage = models.TextField(blank=True, null=True)
    nimserver = models.TextField(blank=True, null=True)
    routenames = models.TextField(blank=True, null=True)
    nameservers = models.TextField(blank=True, null=True)
    proxydhcp = models.TextField(blank=True, null=True)
    syslog = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'noderes'


class Nodetype(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    os = models.TextField(blank=True, null=True)
    arch = models.TextField(blank=True, null=True)
    profile = models.TextField(blank=True, null=True)
    provmethod = models.TextField(blank=True, null=True)
    supportedarchs = models.TextField(blank=True, null=True)
    nodetype = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nodetype'


class Notification(models.Model):
    filename = models.CharField(primary_key=True, max_length=128)
    tables = models.TextField()
    tableops = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notification'


class Openbmc(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    bmc = models.TextField(blank=True, null=True)
    consport = models.TextField(blank=True, null=True)
    taggedvlan = models.TextField(blank=True, null=True)
    username = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'openbmc'


class Osdistro(models.Model):
    osdistroname = models.CharField(primary_key=True, max_length=128)
    basename = models.TextField(blank=True, null=True)
    majorversion = models.TextField(blank=True, null=True)
    minorversion = models.TextField(blank=True, null=True)
    arch = models.TextField(blank=True, null=True)
    type = models.TextField(blank=True, null=True)
    dirpaths = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'osdistro'


class Osdistroupdate(models.Model):
    osupdatename = models.CharField(primary_key=True, max_length=128)
    osdistroname = models.TextField(blank=True, null=True)
    dirpath = models.TextField(blank=True, null=True)
    downloadtime = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'osdistroupdate'


class Osimage(models.Model):
    imagename = models.CharField(primary_key=True, max_length=128)
    groups = models.TextField(blank=True, null=True)
    profile = models.TextField(blank=True, null=True)
    imagetype = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    provmethod = models.TextField(blank=True, null=True)
    rootfstype = models.TextField(blank=True, null=True)
    osdistroname = models.TextField(blank=True, null=True)
    osupdatename = models.CharField(max_length=1024, blank=True, null=True)
    cfmdir = models.TextField(blank=True, null=True)
    osname = models.TextField(blank=True, null=True)
    osvers = models.TextField(blank=True, null=True)
    osarch = models.TextField(blank=True, null=True)
    synclists = models.TextField(blank=True, null=True)
    postscripts = models.TextField(blank=True, null=True)
    postbootscripts = models.TextField(blank=True, null=True)
    serverrole = models.TextField(blank=True, null=True)
    isdeletable = models.TextField(blank=True, null=True)
    kitcomponents = models.TextField(blank=True, null=True)
    environvar = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'osimage'


class Passwd(models.Model):
    key = models.CharField(primary_key=True, max_length=128)
    username = models.CharField(max_length=128)
    password = models.TextField(blank=True, null=True)
    cryptmethod = models.TextField(blank=True, null=True)
    authdomain = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'passwd'
        unique_together = (('key', 'username'),)


class Pdu(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    nodetype = models.TextField(blank=True, null=True)
    pdutype = models.TextField(blank=True, null=True)
    outlet = models.TextField(blank=True, null=True)
    username = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    snmpversion = models.TextField(blank=True, null=True)
    community = models.TextField(blank=True, null=True)
    snmpuser = models.TextField(blank=True, null=True)
    authtype = models.TextField(blank=True, null=True)
    authkey = models.TextField(blank=True, null=True)
    privtype = models.TextField(blank=True, null=True)
    privkey = models.TextField(blank=True, null=True)
    seclevel = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pdu'


class Pduoutlet(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    pdu = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pduoutlet'


class Performance(models.Model):
    timestamp = models.CharField(primary_key=True, max_length=128)
    node = models.CharField(max_length=128)
    attrname = models.CharField(max_length=128)
    attrvalue = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'performance'
        unique_together = (('timestamp', 'node', 'attrname'),)


class Policy(models.Model):
    priority = models.CharField(primary_key=True, max_length=128)
    name = models.TextField(blank=True, null=True)
    host = models.TextField(blank=True, null=True)
    commands = models.TextField(blank=True, null=True)
    noderange = models.TextField(blank=True, null=True)
    parameters = models.TextField(blank=True, null=True)
    time = models.TextField(blank=True, null=True)
    rule = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'policy'


class Postscripts(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    postscripts = models.TextField(blank=True, null=True)
    postbootscripts = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'postscripts'


class Ppc(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    hcp = models.TextField(blank=True, null=True)
    id = models.TextField(blank=True, null=True)
    pprofile = models.TextField(blank=True, null=True)
    parent = models.TextField(blank=True, null=True)
    nodetype = models.TextField(blank=True, null=True)
    supernode = models.TextField(blank=True, null=True)
    sfp = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ppc'


class Ppcdirect(models.Model):
    hcp = models.CharField(primary_key=True, max_length=128)
    username = models.CharField(max_length=128)
    password = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ppcdirect'
        unique_together = (('hcp', 'username'),)


class Ppchcp(models.Model):
    hcp = models.CharField(primary_key=True, max_length=128)
    username = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ppchcp'


class Prescripts(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    begin = models.TextField(blank=True, null=True)
    end = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prescripts'


class Prodkey(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    product = models.CharField(max_length=128)
    key = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prodkey'
        unique_together = (('node', 'product'),)


class Rack(models.Model):
    rackname = models.CharField(primary_key=True, max_length=128)
    displayname = models.TextField(blank=True, null=True)
    num = models.TextField(blank=True, null=True)
    height = models.TextField(blank=True, null=True)
    room = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rack'


class Routes(models.Model):
    routename = models.CharField(primary_key=True, max_length=128)
    net = models.TextField(blank=True, null=True)
    mask = models.TextField(blank=True, null=True)
    gateway = models.TextField(blank=True, null=True)
    ifname = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'routes'


class Servicenode(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    nameserver = models.TextField(blank=True, null=True)
    dhcpserver = models.TextField(blank=True, null=True)
    tftpserver = models.TextField(blank=True, null=True)
    nfsserver = models.TextField(blank=True, null=True)
    conserver = models.TextField(blank=True, null=True)
    monserver = models.TextField(blank=True, null=True)
    ldapserver = models.TextField(blank=True, null=True)
    ntpserver = models.TextField(blank=True, null=True)
    ftpserver = models.TextField(blank=True, null=True)
    nimserver = models.TextField(blank=True, null=True)
    ipforward = models.TextField(blank=True, null=True)
    dhcpinterfaces = models.TextField(blank=True, null=True)
    proxydhcp = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'servicenode'


class Site(models.Model):
    key = models.CharField(primary_key=True, max_length=128)
    value = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'site'


class Statelite(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    image = models.TextField(blank=True, null=True)
    statemnt = models.TextField()
    mntopts = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'statelite'


class Storage(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    osvolume = models.TextField(blank=True, null=True)
    size = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    storagepool = models.TextField(blank=True, null=True)
    hypervisor = models.TextField(blank=True, null=True)
    fcprange = models.TextField(blank=True, null=True)
    volumetag = models.TextField(blank=True, null=True)
    type = models.TextField(blank=True, null=True)
    controller = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'storage'


class Switch(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    switch = models.CharField(max_length=128)
    port = models.CharField(max_length=128)
    vlan = models.TextField(blank=True, null=True)
    interface = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'switch'
        unique_together = (('node', 'switch', 'port'),)


class Switches(models.Model):
    switch = models.CharField(primary_key=True, max_length=128)
    snmpversion = models.TextField(blank=True, null=True)
    username = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    privacy = models.TextField(blank=True, null=True)
    auth = models.TextField(blank=True, null=True)
    linkports = models.TextField(blank=True, null=True)
    sshusername = models.TextField(blank=True, null=True)
    sshpassword = models.TextField(blank=True, null=True)
    protocol = models.TextField(blank=True, null=True)
    switchtype = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'switches'


class Taskstate(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    command = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    pid = models.TextField(blank=True, null=True)
    reserve = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'taskstate'


class Token(models.Model):
    tokenid = models.CharField(primary_key=True, max_length=128)
    username = models.TextField(blank=True, null=True)
    expire = models.TextField(blank=True, null=True)
    created = models.TextField(blank=True, null=True)
    access = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'token'


class Virtsd(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    sdtype = models.TextField(blank=True, null=True)
    stype = models.TextField(blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    host = models.TextField(blank=True, null=True)
    cluster = models.TextField(blank=True, null=True)
    datacenter = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'virtsd'


class Vm(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    mgr = models.TextField(blank=True, null=True)
    host = models.TextField(blank=True, null=True)
    migrationdest = models.TextField(blank=True, null=True)
    storage = models.TextField(blank=True, null=True)
    storagemodel = models.TextField(blank=True, null=True)
    storagecache = models.TextField(blank=True, null=True)
    storageformat = models.TextField(blank=True, null=True)
    cfgstore = models.TextField(blank=True, null=True)
    memory = models.TextField(blank=True, null=True)
    cpus = models.TextField(blank=True, null=True)
    nics = models.TextField(blank=True, null=True)
    nicmodel = models.TextField(blank=True, null=True)
    bootorder = models.TextField(blank=True, null=True)
    clockoffset = models.TextField(blank=True, null=True)
    virtflags = models.TextField(blank=True, null=True)
    master = models.TextField(blank=True, null=True)
    vncport = models.TextField(blank=True, null=True)
    textconsole = models.TextField(blank=True, null=True)
    powerstate = models.TextField(blank=True, null=True)
    beacon = models.TextField(blank=True, null=True)
    datacenter = models.TextField(blank=True, null=True)
    cluster = models.TextField(blank=True, null=True)
    guestostype = models.TextField(blank=True, null=True)
    othersettings = models.TextField(blank=True, null=True)
    physlots = models.TextField(blank=True, null=True)
    vidmodel = models.TextField(blank=True, null=True)
    vidproto = models.TextField(blank=True, null=True)
    vidpassword = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vm'


class Vmmaster(models.Model):
    name = models.CharField(primary_key=True, max_length=128)
    os = models.TextField(blank=True, null=True)
    arch = models.TextField(blank=True, null=True)
    profile = models.TextField(blank=True, null=True)
    storage = models.TextField(blank=True, null=True)
    storagemodel = models.TextField(blank=True, null=True)
    nics = models.TextField(blank=True, null=True)
    vintage = models.TextField(blank=True, null=True)
    originator = models.TextField(blank=True, null=True)
    virttype = models.TextField(blank=True, null=True)
    specializeparameters = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vmmaster'


class Vpd(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    serial = models.TextField(blank=True, null=True)
    mtm = models.TextField(blank=True, null=True)
    side = models.TextField(blank=True, null=True)
    asset = models.TextField(blank=True, null=True)
    uuid = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vpd'


class Websrv(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    port = models.TextField(blank=True, null=True)
    username = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'websrv'


class Winimage(models.Model):
    imagename = models.CharField(primary_key=True, max_length=128)
    template = models.TextField(blank=True, null=True)
    installto = models.TextField(blank=True, null=True)
    partitionfile = models.TextField(blank=True, null=True)
    winpepath = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'winimage'


class Zone(models.Model):
    zonename = models.CharField(primary_key=True, max_length=128)
    sshkeydir = models.TextField(blank=True, null=True)
    sshbetweennodes = models.TextField(blank=True, null=True)
    defaultzone = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'zone'


class Zvm(models.Model):
    node = models.CharField(primary_key=True, max_length=128)
    hcp = models.TextField(blank=True, null=True)
    userid = models.TextField(blank=True, null=True)
    nodetype = models.TextField(blank=True, null=True)
    parent = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)
    discovered = models.TextField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'zvm'


class Zvmivp(models.Model):
    id = models.CharField(primary_key=True, max_length=128)
    ip = models.TextField(blank=True, null=True)
    schedule = models.TextField(blank=True, null=True)
    last_run = models.TextField(blank=True, null=True)
    type_of_run = models.TextField(blank=True, null=True)
    access_user = models.TextField(blank=True, null=True)
    orch_parms = models.TextField(blank=True, null=True)
    prep_parms = models.TextField(blank=True, null=True)
    main_ivp_parms = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    disable = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'zvmivp'
