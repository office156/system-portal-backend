# consumers.py

import json
from channels.generic.websocket import AsyncWebsocketConsumer
from .websocket_manager import connected_clients

class MyConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.accept()
        connected_clients.append(self)

    async def disconnect(self, close_code):
        connected_clients.remove(self)
        pass

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Process the received message as needed
        # Send a response back to the client
        await self.send(text_data=json.dumps({'message': 'You said: ' + message}))

    async def send_message(self, event):
        # Extract the message data from the event
        message = event['message']

        # Process the message as needed
        # ...

        # Send a response to the client
        await self.send(text_data=json.dumps({'response': 'Message received' + message}))
